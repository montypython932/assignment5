/**
 * Written by Phillip Hall, June 2014
 * A class for object House that has three attributes: 
 * wall color, type of floor, number of windows
 * @param houseColor String that stores the color of the house
 * @param floorType String that stores the type of flooring
 * @param numOfWindows Int that stores the number of windows
 */
public class House {
	
	static String houseColor, floorType;
	static int numOfWindows;

	/**First constructor, does nothing
	 * 
	 * @return Empty constructor that does nothing
	 */
	public House() {
		
	}
	
	/**Overloaded constructor, takes three arguments
	 * 
	 * @param color Assigns to houseColor
	 * @param floor Assigns to floorType
	 * @param windows Assigns to numOfWindows
	 */
	public House(String color, String floor, int windows) {
		setColor(color);
		setFloor(floor);
		setWindows(windows);
	}
	
	/**Prints out info about the House object
	 * 
	 */
	public void printHomeInfo() {
		System.out.println("Color: " + getColor() + "\nFloor Type: " + getFloor() + "\nNumber of Windows: " + getWindows() + "\n");
	}
	
	/**Assigns houseColor a value
	 * 
	 * @param color Sets houseColor equal to color
	 */
	public void setColor(String color) {
		houseColor = color;
	}
	
	/**Assigns floorType a value
	 * 
	 * @param floor Sets floorType equal to floor
	 */
	public void setFloor(String floor) {
		floorType = floor;
	}

	/**Assigns numOfWindows a value
	 * 
	 * @param windows Sets numOfWindows equal to windows
	 */
	public void setWindows(int windows) {
		numOfWindows = windows;
	}
	
	/**method that returns value of houseColor
	 * 
	 * @param houseColor is the color of the house
	 */
	public String getColor() {
		return houseColor;
	}
	
	/**method that returns value of floorType
	 * 
	 * @param floorType is the type of flooring a house has
	 */
	public String getFloor() {
		return floorType;
	}

	/**method that returns the value of numOfWindows
	 * 
	 * @param numOfWindows is the number of windows a house has
	 */
	public int getWindows() {
		return numOfWindows;
	}
}
