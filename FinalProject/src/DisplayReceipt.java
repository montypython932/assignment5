/**
 * @author Phillip Hall
 * Class used to display the receipt once done shopping
 */
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.text.DecimalFormat;

public class DisplayReceipt extends JFrame {
	
	DecimalFormat df = new DecimalFormat("0.00");
	JTextArea listShoppingCart = new JTextArea();
	JScrollPane viewReceipt = new JScrollPane(listShoppingCart);

	/**
	 *Constructor 
	 */
	
	public DisplayReceipt() {
		setVisible(true);
		setSize(400, 600);
		setLocationRelativeTo(null);
		this.setBackground(Color.WHITE);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(viewReceipt);
	}
	/**
	 * 
	 * @param products Array of strings that stores products names
	 * @param prices Array of doubles that stores products prices
	 * @param quantity Array of ints that stores products quantities
	 * @param subTotal double that stores the subtotal of all items (before taxes)
	 * @param total double that stores total (subtotal * taxes)
	 */
	public void printOutReceipt(String[] products, double[] prices, int[] quantity, double subTotal, double total) {
		for (int i = 0; i < 9; i++) {
			if (quantity[i] != 0) {
				listShoppingCart.append(products[i] + " x" + quantity[i] + " = " + Double.parseDouble(df.format(prices[i] * quantity[i])) + "\n");
			}
		}
		listShoppingCart.append("Subtotal: " + Double.parseDouble(df.format(subTotal)) + "\n");
		listShoppingCart.append("Total: " + Double.parseDouble(df.format(total)) + "\n");
	}
}