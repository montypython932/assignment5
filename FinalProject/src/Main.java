/**
 * @author Phillip Hall
* Written by Phillip Hall, June 2014
 * Final Project, sign up for account and log in
 * then add items to the shopping cart and purchase them.
 * This class displays sign up/log in JFrame
 */

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main extends JFrame implements ActionListener {
	
		private JButton signUp, logIn;
		private JTextField signUpUser, logInUser;
		private JPasswordField signUpPass1, signUpPass2, logInPass;
		private JLabel output;
		public Main gui;
		public Registration register = new Registration();
		public Logger logg = new Logger();
	
		/**
		 * @
		 * Constructor for making the sign up/ log in box
		 */
		
		public Main() {
			setVisible(true);
			setSize(300, 240);
			setLocationRelativeTo(null);
			this.setBackground(Color.WHITE);
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			this.signUp = new JButton("Sign Up");
			this.logIn = new JButton("Log In");
			this.signUpUser = new JTextField(20);
			this.signUpPass1 = new JPasswordField(20);
			this.signUpPass2 = new JPasswordField(20);
			this.logInUser = new JTextField(20);
			this.logInPass = new JPasswordField(20);
			this.output = new JLabel();
			setLayout(new FlowLayout());
			add(signUp);
			signUp.addActionListener(this);
			add(signUpUser);
			add(signUpPass1);
			add(signUpPass2);
			add(logIn);
			logIn.addActionListener(this);
			add(logInUser);
			add(logInPass);
			add(output);
		}
		/**
		 * Main method, creates new instance of object called main
		 */
		public static void main(String[] args) {
			Main gui = new Main();
		}
		/**
		 * Used to handle various inputs from fields and buttons
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == logIn) {
				String [] names = register.usernames;
				String [] passes = register.passwords;
				Authentication auth = new Authentication(names, passes, logInUser.getText(), logInPass.getText());
				//if username/password is correct, continue
				if (auth.authenticate() == true) {
					Interface storefront = new Interface();
					storefront.getObject(logg);
					logg.addToLog("User " + logInUser.getText() + " succesfully logged in");
				}//else display invalid login
				if (auth.authenticate() == false) {
					output.setText("Invalid Password/Username Combo");
					logg.addToLog("Unsuccessful login, Username: " + logInUser.getText() + " Password: " + logInPass.getText());
				}
			}
			if (e.getSource() == signUp) {
				if (signUpUser.getText().length() >= 3 && signUpPass1.getText() != null && signUpPass1.getText().equals(signUpPass2.getText())) {
					register.addNewUser(signUpUser.getText(), signUpPass1.getText());
					output.setText("You've Created An Account!");
				}
			}
		}
		
}