/**
 * @author Phillip Hall
 * Class used to display log of what user is doing
 */

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class DisplayLog extends JFrame {
	
	JTextArea logDisplay = new JTextArea();
	JScrollPane view = new JScrollPane(logDisplay);
	
	/**
	 * Constructor
	 */

	public DisplayLog() {
		setVisible(true);
		setSize(400, 600);
		setLocationRelativeTo(null);
		this.setBackground(Color.WHITE);
		add(view);
	}

	/**
	 * 
	 * @param userLog An array of strings that stores what the user has been doing
	 */
	public void printOutLog(String[] userLog) {
		for (int i = 0; i < 997; i++) {
			if (userLog [i] != null) {
				logDisplay.append(userLog[i] + "\n");
			}
		}
	}
}
