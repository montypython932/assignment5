/**
 * @author Phillip Hall
 *Class used for authenticating user log in
 */

public class Authentication {
	
	String [] username;
	String [] password;
	String userIn;
	String passIn;
	
	/**
	 * Default Constructor
	 */
	
	public Authentication() {
		
	}
	
	/**
	 * Overloaded Constructor
	 * @param usernames An array that stores usernames for all accounts
	 * @param passwords An array that stores passwords for all accounts
	 * @param userInput Input received for username, compared against usernames[]
	 * @param passInput Input received for password, compared against passwords[]
	 */
	
	public Authentication(String [] usernames, String [] passwords, String userInput, String passInput) {
		username = usernames;
		password = passwords;
		userIn = userInput;
		passIn = passInput;
	}

	/**
	 * Determines if input received for username/password is valid
	 * @return True if correct login, false otherwise
	 */
	
	public boolean authenticate() {
		for (int i = 0; i < 50; i++) {
			if (username[i] != null && password[i] != null) {
				if (username[i].equals(userIn) && password[i].equals(passIn)) {
					return true;
				}
			}
		}
		return false;
	}
}