/**
 * 
 * @author Phillip Hall
 * Class for logging what the user does
 */
public class Logger {

	String[] userLog = new String[1000];
	int counter = 0;
	
	/**
	 * Constructor
	 */
	public Logger() {
		
	}
	/**
	 * Adds a message to the array userLog[] used to log what the user is doing
	 * @param input A message about what the user is doing
	 */
	public void addToLog(String input) {
		userLog[counter] = input;
		counter++;
	}
}
